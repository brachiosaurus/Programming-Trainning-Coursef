import time
import requests
from requests.exceptions import HTTPError

for url in ['http://api.github.com', 'https://api.github.com/invalid']:
    try:
        response = requests.get(url)

        # If the response was successful, no Exception will be raised
        response.raise_for_status()
    except HTTPError as http_errs:
        print(f'HTTP error occurred: {http_errs}')  # Python 3.6
    except Exception as err:
        print(f'Other error occurred: {err}')  # Python 3.6
    else:
        print('Success!')


print("You have come a long way, take some breath now")
time.sleep(True*10)

print("Head to the next bonfire")
